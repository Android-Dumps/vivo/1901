## full_k62v1_64_bsp-user 11 RP1A.200720.012 eng.compil.20211009.184437 release-keys
- Manufacturer: vivo
- Platform: mt6765
- Codename: 1901
- Brand: vivo
- Flavor: full_k62v1_64_bsp-user
- Release Version: 11
- Id: RP1A.200720.012
- Incremental: eng.compil.20211009.184437
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 320
- Fingerprint: vivo/1901/1901:11/RP1A.200720.012/compiler1009184113:user/release-keys
- OTA version: 
- Branch: full_k62v1_64_bsp-user-11-RP1A.200720.012-eng.compil.20211009.184437-release-keys
- Repo: vivo/1901
