#
# Copyright (C) 2021 The Android Open Source Project
# Copyright (C) 2021 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_1901.mk

COMMON_LUNCH_CHOICES := \
    omni_1901-user \
    omni_1901-userdebug \
    omni_1901-eng
